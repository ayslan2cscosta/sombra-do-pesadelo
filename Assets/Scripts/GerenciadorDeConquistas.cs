using UnityEngine;
using System.Collections.Generic;

public static class GerenciadorDeConquistas
{
    // Conquistas do primeiro cen�rio
    public static bool encontrouBichinhosDePelucia;
    public static bool saiuDoAto1SemPerderSanidade;
    public static bool interagiuComAssombracaoDoAto1;
    public static bool encontrarCoelhoAmaldi�oado;
    public static bool encontrouFacaEnferrujada;

    // Conquistas do segundo cen�rio
    public static bool naoPerdeuSanidadeNoAto2;
    public static bool naoPisouEmArmadilhas;
    public static bool saiuDaMansao;

    // Fun��o gen�rica para desbloquear uma conquista
    public static void DesbloquearConquista(string nomeConquista)
    {
        switch (nomeConquista)
        {
            case "Encontrar todos os bichinhos de pel�cia":
                Debug.Log("Conquista desbloqueada: Abra�o de Pel�cia");
                encontrouBichinhosDePelucia = true;
                break;
            case "Sair do ato 1 sem perder pontos de sanidade":
                Debug.Log("Conquista desbloqueada: Sanidade � Prova de Susto");
                saiuDoAto1SemPerderSanidade = true;
                break;
            case "Interagir com a assombra��o do ato 1":
                Debug.Log("Conquista desbloqueada: Papo com o al�m");
                interagiuComAssombracaoDoAto1 = true;
                break;
            case "Encontrar a faca enferrujada":
                Debug.Log("Conquista desbloqueada: Lembran�a");
                encontrouFacaEnferrujada = true;
                break;
            case "N�o perder ponto de sanidade no ato 2":
                Debug.Log("Conquista desbloqueada: Mente s�");
                naoPerdeuSanidadeNoAto2 = true;
                break;
            case "Encontrar o coelho amaldi�oado":
                Debug.Log("Conquista desbloqueada: De fofo n�o tem nada");
                encontrarCoelhoAmaldi�oado = true;
                break;
            case "N�o pisar em nenhuma armadilha":
                Debug.Log("Conquista desbloqueada: Rastro limpo");
                naoPisouEmArmadilhas = true;
                break;
            case "Sair da mans�o":
                Debug.Log("Conquista desbloqueada: Fim?");
                saiuDaMansao = true;
                break;
            default:
                Debug.LogWarning("Conquista n�o encontrada: " + nomeConquista);
                break;
        }
    }

    // Verifica se todas as conquistas do primeiro cen�rio foram desbloqueadas
    public static bool ConquistasDoPrimeiroCenarioCompletas()
    {
        return encontrouBichinhosDePelucia &&
               saiuDoAto1SemPerderSanidade &&
               interagiuComAssombracaoDoAto1 &&
               encontrouFacaEnferrujada;
    }

    // Verifica se todas as conquistas do segundo cen�rio foram desbloqueadas
    public static bool ConquistasDoSegundoCenarioCompletas()
    {
        return naoPerdeuSanidadeNoAto2 &&
               encontrarCoelhoAmaldi�oado &&
               naoPisouEmArmadilhas &&
               saiuDaMansao;
    }

    // Aqui voc� pode adicionar m�todos para outras verifica��es espec�ficas necess�rias para desbloquear conquistas
}
