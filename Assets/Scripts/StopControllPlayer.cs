using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StopControllPlayer : MonoBehaviour
{
    [SerializeField] Anna_PlayerMovement playerController; // Refer�ncia ao script de controle do jogador
    [SerializeField] Animator playerAnimator; // Refer�ncia ao componente Animator do jogador

    [SerializeField] Camera AnnaCamera;
    [SerializeField] Camera SkeletonCamera;
    [SerializeField] Camera GhostCamera;

    [SerializeField] GameObject KnifeplayerObject; // Objeto dentro do jogador
    [SerializeField] GameObject KnifesceneObject; // Objeto no cen�rio
    [SerializeField] GameObject KnifeControllerObject; // Objeto no cen�rio

    [SerializeField] GameObject PigplayerObject; // Objeto dentro do jogador
    [SerializeField] GameObject PigsceneObject; // Objeto no cen�rio
    [SerializeField] GameObject PigControllerObject; // Objeto no cen�rio

    [SerializeField] GameObject BearplayerObject; // Objeto dentro do jogador
    [SerializeField] GameObject BearsceneObject; // Objeto no cen�rio
    [SerializeField] GameObject BearControllerObject; // Objeto no cen�rio

    [SerializeField] GameObject MonkeyplayerObject; // Objeto dentro do jogador
    [SerializeField] GameObject MonkeysceneObject; // Objeto no cen�rio
    [SerializeField] GameObject MonkeyControllerObject; // Objeto no cen�rio

    [SerializeField] GameObject BunnyplayerObject; // Objeto dentro do jogador
    [SerializeField] GameObject BunnysceneObject; // Objeto no cen�rio
    [SerializeField] GameObject BunnyControllerObject; // Objeto no cen�rio

    [SerializeField] GameObject SheepplayerObject; // Objeto dentro do jogador
    [SerializeField] GameObject SheepsceneObject; // Objeto no cen�rio
    [SerializeField] GameObject SheepControllerObject; // Objeto no cen�rio

    [SerializeField] GameObject PenguimplayerObject; // Objeto dentro do jogador
    [SerializeField] GameObject PenguimsceneObject; // Objeto no cen�rio
    [SerializeField] GameObject PenguimControllerObject; // Objeto no cen�rio

    [SerializeField] GameObject DoorControllerObject; // Objeto no cen�rio


    [SerializeField] GameObject FinalCamera;
    [SerializeField] GameObject FinalObject; // Objeto dentro do jogador
    [SerializeField] GameObject FinalAnnaDisableObject; // Objeto dentro do jogador
    [SerializeField] GameObject ProgressBarObject; // Objeto dentro do jogador
    [SerializeField] GameObject FinalTrigger5Object; // Objeto dentro do jogador

    public float fadeDuration = 1f;
    public float displayImageDuration = 14f;

    public CanvasGroup caughtBackgroundImageCanvasGroup;
    public AudioSource caughtAudio;
    [SerializeField] GameObject caughtAudioObject;
    private bool imageTransition = false;
    private float timer = 0f;
    private bool m_HasAudioPlayed = false;

    public AudioSource audioSource;

    // M�todo para desativar o AudioSource
    public void DisableAudio()
    {
        if (audioSource != null)
        {
            audioSource.enabled = false;
        }
    }

    // M�todo para ativar o AudioSource
    public void EnableAudio()
    {
        if (audioSource != null)
        {
            audioSource.enabled = true;
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (imageTransition) {
            UpdateFade();
        }
    }

    public void StopController()
    {
        playerController.SetPlayerActive(false);
    }

    public void StartController()
    {
        playerController.SetPlayerActive(true);
    }

    public void CameraAnnaOn()
    {
        AnnaCamera.gameObject.SetActive(true);
    }

    public void CameraAnnaOff()
    {
        AnnaCamera.gameObject.SetActive(false);
    }

    public void CameraSkeletonOn()
    {
        SkeletonCamera.gameObject.SetActive(true);
    }

    public void CameraSkeletonOff()
    {
        SkeletonCamera.gameObject.SetActive(false);
    }

    public void CameraGhostOn()
    {
        GhostCamera.gameObject.SetActive(true);
    }

    public void CameraGhostOff()
    {
        GhostCamera.gameObject.SetActive(false);
    }

    public void StartInteractingAnimation()
    {
        // Ativa a anima��o "isInteracting" do jogador
        playerAnimator.SetBool("isInteracting", true);
    }

    public void StopInteractingAnimation()
    {
        // Desativa a anima��o "isInteracting" do jogador
        playerAnimator.SetBool("isInteracting", false);
    }

    //Pig ----------------------------------------------------------------------------------
    public void PIGSwitchObjects()
    {
        StartCoroutine(SwitchObjectsWithDelay());
    }

    private IEnumerator SwitchObjectsWithDelay()
    {
        // Espera por 0.25 segundos (250ms)
        yield return new WaitForSeconds(1.4f);

        // Desabilita o objeto no cen�rio
        PigsceneObject.SetActive(false);

        // Espera por 0.25 segundos (250ms)
        yield return new WaitForSeconds(0.5f);

        // Habilita o objeto dentro do jogador
        PigplayerObject.SetActive(true);
    }

    public void PIGDisablePlayerObject()
    {
        // Desabilita o objeto no cen�rio
        PigplayerObject.SetActive(false);
        
    }
    public void PIGDisableControllerObject()
    {
        // Move o objeto para cima para n�o ser vis�vel
        Vector3 newPosition = PigControllerObject.transform.position + Vector3.up * 30;
        PigControllerObject.transform.position = newPosition;
    }

    //Bear ---------------------------------------------------------------------------------
    public void BearSwitchObjects()
    {
        StartCoroutine(SwitchObjectsWithDelay2());
    }

    private IEnumerator SwitchObjectsWithDelay2()
    {
        // Espera por 0.25 segundos (250ms)
        yield return new WaitForSeconds(1.4f);

        // Desabilita o objeto no cen�rio
        BearsceneObject.SetActive(false);

        // Espera por 0.25 segundos (250ms)
        yield return new WaitForSeconds(0.5f);

        // Habilita o objeto dentro do jogador
        BearplayerObject.SetActive(true);
    }

    public void BearDisablePlayerObject()
    {
        // Desabilita o objeto no cen�rio
        BearplayerObject.SetActive(false);

    }
    public void BearDisableControllerObject()
    {
        // Move o objeto para cima para n�o ser vis�vel
        Vector3 newPosition = BearControllerObject.transform.position + Vector3.up * 30;
        BearControllerObject.transform.position = newPosition;
    }
    //Monkey -------------------------------------------------------------------------------
    public void MonkeySwitchObjects()
    {
        StartCoroutine(SwitchObjectsWithDelay3());
    }

    private IEnumerator SwitchObjectsWithDelay3()
    {
        // Espera por 0.25 segundos (250ms)
        yield return new WaitForSeconds(1.4f);

        // Desabilita o objeto no cen�rio
        MonkeysceneObject.SetActive(false);

        // Espera por 0.25 segundos (250ms)
        yield return new WaitForSeconds(0.5f);

        // Habilita o objeto dentro do jogador
        MonkeyplayerObject.SetActive(true);
    }

    public void MonkeyDisablePlayerObject()
    {
        // Desabilita o objeto no cen�rio
        MonkeyplayerObject.SetActive(false);

    }
    public void MonkeyDisableControllerObject()
    {
        // Move o objeto para cima para n�o ser vis�vel
        Vector3 newPosition = MonkeyControllerObject.transform.position + Vector3.up * 30;
        MonkeyControllerObject.transform.position = newPosition;
    }
    //Bunny --------------------------------------------------------------------------------
    public void BunnySwitchObjects()
    {
        StartCoroutine(SwitchObjectsWithDelay4());
    }

    private IEnumerator SwitchObjectsWithDelay4()
    {
        // Espera por 0.25 segundos (250ms)
        yield return new WaitForSeconds(1.4f);

        // Desabilita o objeto no cen�rio
        BunnysceneObject.SetActive(false);

        // Espera por 0.25 segundos (250ms)
        yield return new WaitForSeconds(0.5f);

        // Habilita o objeto dentro do jogador
        BunnyplayerObject.SetActive(true);
    }

    public void BunnyDisablePlayerObject()
    {
        // Desabilita o objeto no cen�rio
        BunnyplayerObject.SetActive(false);

    }
    public void BunnyDisableControllerObject()
    {
        // Move o objeto para cima para n�o ser vis�vel
        Vector3 newPosition = BunnyControllerObject.transform.position + Vector3.up * 30;
        BunnyControllerObject.transform.position = newPosition;
    }
    //Sheep --------------------------------------------------------------------------------
    public void SheepSwitchObjects()
    {
        StartCoroutine(SwitchObjectsWithDelay5());
    }

    private IEnumerator SwitchObjectsWithDelay5()
    {
        // Espera por 0.25 segundos (250ms)
        yield return new WaitForSeconds(1.4f);

        // Desabilita o objeto no cen�rio
        SheepsceneObject.SetActive(false);

        // Espera por 0.25 segundos (250ms)
        yield return new WaitForSeconds(0.5f);

        // Habilita o objeto dentro do jogador
        SheepplayerObject.SetActive(true);
    }

    public void SheepDisablePlayerObject()
    {
        // Desabilita o objeto no cen�rio
        SheepplayerObject.SetActive(false);

    }
    public void SheepDisableControllerObject()
    {
        // Move o objeto para cima para n�o ser vis�vel
        Vector3 newPosition = SheepControllerObject.transform.position + Vector3.up * 30;
        SheepControllerObject.transform.position = newPosition;
    }
    //Penguim ------------------------------------------------------------------------------
    public void PenguimSwitchObjects()
    {
        StartCoroutine(SwitchObjectsWithDelay6());
    }

    private IEnumerator SwitchObjectsWithDelay6()
    {
        // Espera por 0.25 segundos (250ms)
        yield return new WaitForSeconds(1.4f);

        // Desabilita o objeto no cen�rio
        PenguimsceneObject.SetActive(false);

        // Espera por 0.25 segundos (250ms)
        yield return new WaitForSeconds(0.5f);

        // Habilita o objeto dentro do jogador
        PenguimplayerObject.SetActive(true);
    }

    public void PenguimDisablePlayerObject()
    {
        // Desabilita o objeto no cen�rio
        PenguimplayerObject.SetActive(false);

    }
    public void PenguimDisableControllerObject()
    {
        // Move o objeto para cima para n�o ser vis�vel
        Vector3 newPosition = PenguimControllerObject.transform.position + Vector3.up * 30;
        PenguimControllerObject.transform.position = newPosition;
    }

    public void DoorDisableControllerObject()
    {
        // Move o objeto para cima para n�o ser vis�vel
        Vector3 newPosition = DoorControllerObject.transform.position + Vector3.up * 30;
        DoorControllerObject.transform.position = newPosition;
    }

    //Knife ----------------------------------------------------------------------------------
    public void KnifeSwitchObjects()
    {
        StartCoroutine(KnifeSwitchObjectsWithDelay());
    }

    private IEnumerator KnifeSwitchObjectsWithDelay()
    {
        // Espera por 0.25 segundos (250ms)
        yield return new WaitForSeconds(1.4f);

        // Desabilita o objeto no cen�rio
        KnifesceneObject.SetActive(false);

        // Espera por 0.25 segundos (250ms)
        yield return new WaitForSeconds(0.5f);

        // Habilita o objeto dentro do jogador
        KnifeplayerObject.SetActive(true);
    }

    public void KnifeDisablePlayerObject()
    {
        // Desabilita o objeto no cen�rio
        KnifeplayerObject.SetActive(false);

    }
    public void KnifeDisableControllerObject()
    {
        // Move o objeto para cima para n�o ser vis�vel
        Vector3 newPosition = KnifeControllerObject.transform.position + Vector3.up * 30;
        KnifeControllerObject.transform.position = newPosition;
    }

    //Knife ----------------------------------------------------------------------------------
    public void FinalSwitchObjects()
    {
        StartCoroutine(FinalSwitchObjectsWithDelay());

        
    }

    private IEnumerator FinalSwitchObjectsWithDelay()
    {

        // Move o objeto para cima para n�o ser vis�vel
        Vector3 newPosition = KnifeControllerObject.transform.position + Vector3.up * 30;
        KnifeControllerObject.transform.position = newPosition;

        ProgressBarObject.SetActive(false);

        // Espera por 0.25 segundos (250ms)
        yield return new WaitForSeconds(0.5f);

        FinalCamera.SetActive(false);
        // Habilita o objeto dentro do jogador
        FinalObject.SetActive(true);
        // Habilita o objeto dentro do jogador
        FinalAnnaDisableObject.SetActive(false);

        

        // Espera por 0.25 segundos (250ms)
        yield return new WaitForSeconds(15f);

        // Habilita o objeto dentro do jogador
        FinalObject.SetActive(false);
        //FinalCamera.SetActive(true);
        FinalTrigger5Object.SetActive(false);
        // Habilita o objeto dentro do jogador
        FinalAnnaDisableObject.SetActive(true);
        AnnaCamera.gameObject.SetActive(true);
        imageTransition = true;

        //SceneManager.LoadScene("Scene_02"); // Carrega a cena de destino
        

    }

    private void UpdateFade()
    {
        if (!m_HasAudioPlayed)
        {
            caughtAudioObject.SetActive(true);
            caughtAudio.Play();
            m_HasAudioPlayed = true;
        }

        timer += Time.deltaTime;
        caughtBackgroundImageCanvasGroup.alpha = timer / fadeDuration;

        if (timer > fadeDuration + displayImageDuration)
        {

            // Carrega a cena principal
            SceneManager.LoadScene("Scene_02", LoadSceneMode.Single);

        }
    }

    public void FinalDisablePlayerObject()
    {
        // Desabilita o objeto no cen�rio
        KnifeplayerObject.SetActive(false);

    }
    public void FinalDisableControllerObject()
    {
        // Move o objeto para cima para n�o ser vis�vel
        Vector3 newPosition = KnifeControllerObject.transform.position + Vector3.up * 30;
        KnifeControllerObject.transform.position = newPosition;
    }
}
