using UnityEngine;

public class DialogueCounter : MonoBehaviour
{
    public float count = 0;
    public ObjectAndLightControlScript objectAndLightControlScript;

    public void Count()
    {
        count++;
        Debug.Log(count);
        if (count == 16)
        {
            // Chamar o m�todo de outro script para inicializar a rotina
            if (objectAndLightControlScript != null)
            {
                objectAndLightControlScript.StartRotina();
            }
            else
            {
                Debug.LogError("ObjectAndLightControlScript n�o atribu�do ao DialogueCounter.");
            }
        }
    }
}
