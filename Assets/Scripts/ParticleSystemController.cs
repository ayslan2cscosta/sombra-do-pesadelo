using UnityEngine;

public class ParticleSystemController : MonoBehaviour
{
    [SerializeField] private ParticleSystem particles; // Refer�ncia para o Particle System
    [SerializeField] private GameObject objectToDisable; // Objeto a ser desativado ap�s 2 segundos
    [SerializeField] private GameObject objectToDisable2; // Objeto a ser desativado ap�s 2 segundos
    [SerializeField] private float emissionDecreaseRate = 1f; // Taxa de redu��o da emiss�o das part�culas por segundo

    private bool isDisablingObject = false;
    private float initialEmissionRate;

    void Start()
    {
        // Verifica se foi atribu�do um Particle System
        if (particles == null)
        {
            Debug.LogError("Particle System n�o atribu�do ao ParticleSystemController.");
            return;
        }

        // Desativa o Particle System inicialmente
        particles.Stop();
        particles.Clear();
    }

    void Update()
    {
        // Se estamos desativando o objeto, reduz gradualmente a taxa de emiss�o das part�culas
        if (isDisablingObject)
        {
            var emission = particles.emission;
            var currentRate = emission.rateOverTime.constant;
            var newRate = Mathf.Max(0f, currentRate - (emissionDecreaseRate * Time.deltaTime));
            emission.rateOverTime = new ParticleSystem.MinMaxCurve(newRate);

            // Se a taxa de emiss�o for zero, desativa o objeto
            if (newRate <= 0f)
            {
                objectToDisable.SetActive(false);
                objectToDisable2.SetActive(false);
                isDisablingObject = false;
            }
        }
    }

    public void TriggerEffect()
    {
        // Ativa o Particle System
        particles.Play();

        // Desativa o objeto ap�s 2 segundos
        if (objectToDisable != null)
        {
            Invoke("DisableObject", 0.1f);
        }
    }

    void DisableObject()
    {
        isDisablingObject = true;
    }
}
