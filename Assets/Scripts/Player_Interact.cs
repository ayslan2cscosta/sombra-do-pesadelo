using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Interact : MonoBehaviour
{
    [SerializeField] Anna_PlayerMovement playerController; // ReferÍncia ao script de controle do jogador

    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            playerController.SetPlayerActive(false);

            float interactRange = 1f;
            Collider[] colliderArray = Physics.OverlapSphere(transform.position, interactRange);
            foreach (Collider collider in colliderArray)
            {
                if(collider.TryGetComponent(out NPCInteractable npcsInteractable)){
                    
                    npcsInteractable.Interact();
                }
            }
        }
    }


    public NPCInteractable GetInteractableObject() {

        float interactRange = 1f;
        Collider[] colliderArray = Physics.OverlapSphere(transform.position, interactRange);
        foreach (Collider collider in colliderArray)
        {
            if (collider.TryGetComponent(out NPCInteractable npcsInteractable))
            {

                return npcsInteractable;
            }
        }
        return null;    
    }
}
