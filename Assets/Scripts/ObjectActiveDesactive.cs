using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class ObjectActiveDesactive : MonoBehaviour
{
    [SerializeField] Anna_PlayerMovement playerController; // Refer�ncia ao script de controle do jogador
    public GameObject objectToActivate;
    public GameObject objectToDeactivate1;
    public GameObject audio;
    public Camera cameraToDeactivate;
    [SerializeField] GameObject ControllerObject;
    private float count=0;

    public void Counter()
    {
        count++;
        Debug.Log(count);
        if (count == 1)
        {
            StartRotina();

        }
    }

    public void StartRotina()
    {
        StartCoroutine(ActivationAndLightIntensityRoutine());
    }

    IEnumerator ActivationAndLightIntensityRoutine()
    {
        // Move o objeto para cima para n�o ser vis�vel
        Vector3 newPosition = ControllerObject.transform.position + Vector3.up * 30;
        ControllerObject.transform.position = newPosition;

        // Ativar os objetos
        objectToActivate.SetActive(true);
        objectToDeactivate1.SetActive(false);
        audio.SetActive(false);
        cameraToDeactivate.enabled = false;

        // Alterar a intensidade da luz ap�s 7 segundos
        yield return new WaitForSeconds(80);

        // Inverter a ativa��o
        objectToActivate.SetActive(false);
        objectToDeactivate1.SetActive(true);
        audio.SetActive(true);  
        cameraToDeactivate.enabled = true;

        playerController.SetPlayerActive(true);
    }
}
