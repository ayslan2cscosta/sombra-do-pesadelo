using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneOnClick : MonoBehaviour
{

    public GameObject objectToEnable;
    public GameObject objectToDisable1;
    public GameObject objectToDisable2;
    public GameObject desabilitarCenario;
    public GameObject objectToEnableAlt;

    public GameObject ControlPanel;
    public GameObject ControlLine;
    public GameObject ConquistaPanel;
    public GameObject ConquistaLine;

    public void LoadScene()
    {
        SceneManager.LoadScene("Scene_00", LoadSceneMode.Single);
    }

    //Jogar - abrir no jogo
    public void JogarAbrirJogo()
    {
        objectToEnable.SetActive(true);
    }

    //Desabilitar novo jogo, main e habilitar config
    public void IrParaConfig()
    {
        objectToDisable1.SetActive(false);
        objectToDisable2.SetActive(false);
        objectToEnableAlt.SetActive(true);
        desabilitarCenario.SetActive(false);
    }

    public void VoltarMain()
    {
        //objectToDisable1.SetActive(true);
        objectToDisable2.SetActive(true);
        objectToEnableAlt.SetActive(false);
        desabilitarCenario.SetActive(true);
    }

    public void HabilitarControles()
    {
        ControlPanel.SetActive(true);
        ControlLine.SetActive(true);
        ConquistaPanel.SetActive(false);
        ConquistaLine.SetActive(false);
    }

    public void HabilitarConquistas()
    {
        ControlPanel.SetActive(false);
        ControlLine.SetActive(false);
        ConquistaPanel.SetActive(true);
        ConquistaLine.SetActive(true);
    }

    public void DisableAndEnableObject()
    {
        objectToDisable1.SetActive(true);
        objectToEnableAlt.SetActive(true);
    }

    public void CloseGame()
    {
        Application.Quit();
    }
}
