using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField] private AudioSource audioSource1; // Primeiro AudioSource
    [SerializeField] private AudioSource audioSource2; // Segundo AudioSource

    [SerializeField] private AudioSource audioSourceToys; // Segundo AudioSource

    [SerializeField] private AudioClip audioClip1; // Primeiro AudioClip
    [SerializeField] private AudioClip audioClip2; // Segundo AudioClip

    [SerializeField] private AudioClip audioClipToys; // Segundo AudioClip

    public void PlayAudio1()
    {
        if (audioClip1 != null && audioSource1 != null)
        {
            audioSource1.clip = audioClip1;
            audioSource1.Play();
        }
        else
        {
            Debug.LogWarning("AudioClip 1 ou AudioSource 1 n�o est� configurado.");
        }
    }

    public void PlayAudio2()
    {
        if (audioClip2 != null && audioSource2 != null)
        {
            audioSource2.clip = audioClip2;
            audioSource2.Play();
        }
        else
        {
            Debug.LogWarning("AudioClip 2 ou AudioSource 2 n�o est� configurado.");
        }
    }

    public void PlayAudioToys()
    {
        if (audioClipToys != null && audioSourceToys != null)
        {
            audioSourceToys.clip = audioClipToys;
            audioSourceToys.Play();
        }
        else
        {
            Debug.LogWarning("AudioClip 2 ou AudioSource 2 n�o est� configurado.");
        }
    }
}
