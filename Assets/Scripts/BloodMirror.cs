using UnityEngine;

public class BloodMirror : MonoBehaviour
{
    public Material materialDoObjeto;
    public float alphaInvisivel = 0f;
    public float alphaVisivel = 1f;
    private bool objetoVisivel = false;

    void Start()
    {
        // Torna o objeto invis�vel no in�cio
        SetAlpha(alphaInvisivel);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (!objetoVisivel)
            {
                // Torna o objeto vis�vel quando o jogador colide com ele pela primeira vez
                SetAlpha(alphaVisivel);
                objetoVisivel = true;
            }
        }
    }

    private void SetAlpha(float alpha)
    {
        Color corDoMaterial = materialDoObjeto.color;
        corDoMaterial.a = alpha;
        materialDoObjeto.color = corDoMaterial;
    }
}
