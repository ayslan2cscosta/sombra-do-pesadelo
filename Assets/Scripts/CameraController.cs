using UnityEngine;
using Cinemachine;

public class CameraController : MonoBehaviour
{
    public Camera defaultCamera;
    public LayerMask triggerLayer;
    public float maxRaycastDistance = 10f; // Dist�ncia m�xima do raio

    private CinemachineVirtualCamera lastActivatedCamera;

    private void Update()
    {
        // Ponto de origem do raio (posi��o do jogador)
        Vector3 origin = transform.position;

        // Dire��o do raio (para frente)
        Vector3 direction = transform.forward;

        // Objeto que armazenar� informa��es sobre o que o raio atingiu
        RaycastHit hit;

        // Lan�a o raio com uma dist�ncia m�xima
        if (Physics.Raycast(origin, direction, out hit, maxRaycastDistance, triggerLayer))
        {
            // Se o raio atingir um objeto de gatilho com a layer correta
            // Ative a c�mera correspondente ao trigger
            CinemachineVirtualCamera virtualCamera = hit.collider.GetComponent<CinemachineVirtualCamera>();
            if (virtualCamera != null && virtualCamera != lastActivatedCamera)
            {
                ActivateVirtualCamera(virtualCamera);
                lastActivatedCamera = virtualCamera;
            }
        }
        else
        {
            // Se o raio n�o atingir nenhum objeto de gatilho, ative a c�mera padr�o
            ActivateMainCamera();
            lastActivatedCamera = null;
        }
    }

    private void ActivateMainCamera()
    {
        // Desativa todas as c�meras virtuais
        CinemachineVirtualCamera[] allCameras = FindObjectsOfType<CinemachineVirtualCamera>();
        foreach (var cam in allCameras)
        {
            cam.gameObject.SetActive(false);
        }

        // Ativa a c�mera principal
        if (defaultCamera != null)
        {
            defaultCamera.gameObject.SetActive(true);
        }
    }

    private void ActivateVirtualCamera(CinemachineVirtualCamera camera)
    {
        // Desativa todas as c�meras virtuais
        CinemachineVirtualCamera[] allCameras = FindObjectsOfType<CinemachineVirtualCamera>();
        foreach (var cam in allCameras)
        {
            cam.gameObject.SetActive(false);
        }

        // Ativa a c�mera virtual passada como par�metro
        if (camera != null)
        {
            camera.gameObject.SetActive(true);
        }
    }
}
