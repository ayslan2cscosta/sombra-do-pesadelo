using System.Collections;
using UnityEngine;

public class EnemyTeleportController : MonoBehaviour
{
    public GameObject enemyPrefab; // Prefab do inimigo
    public Transform[] patrolPoints; // Pontos de patrulha para o inimigo andar
    public AudioClip audioClip; // Clip de �udio a ser reproduzido
    public float movementSpeed = 3f; // Velocidade de movimento do inimigo

    private GameObject currentEnemy; // Refer�ncia ao inimigo atual
    private int currentPatrolIndex = 0; // �ndice do ponto de patrulha atual
    private Animator enemyAnimator; // Componente Animator do inimigo
    private bool isPatrolling = false; // Flag para indicar se o inimigo est� patrulhando

    void OnTriggerEnter(Collider other)
    {
        // Se o jogador colidir com o trigger, inicie o teletransporte e a patrulha
        if (other.CompareTag("Player"))
        {
            ActivateEnemy();
        }
    }

    void ActivateEnemy()
    {
        // Instancia o inimigo e o coloca na primeira posi��o de patrulha
        currentEnemy = Instantiate(enemyPrefab, patrolPoints[0].position, Quaternion.identity);
        isPatrolling = true;

        // Obt�m o componente Animator do inimigo
        enemyAnimator = currentEnemy.GetComponent<Animator>();

        // Inicia a anima��o de caminhada
        if (enemyAnimator != null)
        {
            enemyAnimator.SetBool("IsWalking", true);
        }

        // Reproduz o �udio e agendamento da desativa��o do trigger ap�s o t�rmino do �udio
        if (audioClip != null)
        {
            AudioSource audioSource = currentEnemy.AddComponent<AudioSource>();
            audioSource.PlayOneShot(audioClip);
            StartCoroutine(DeactivateTriggerDelayed(audioClip.length + 5));
        }
    }

    IEnumerator DeactivateTriggerDelayed(float delay)
    {
        yield return new WaitForSeconds(delay);
        gameObject.SetActive(false); // Desativa o objeto de trigger
    }

    void Update()
    {
        // Se o inimigo estiver patrulhando, mova-se em dire��o ao ponto de patrulha atual
        if (isPatrolling)
        {
            MoveToPatrolPoint();
        }
    }

    void MoveToPatrolPoint()
    {
        // Calcula a dire��o para o pr�ximo ponto de patrulha
        Vector3 direction = (patrolPoints[currentPatrolIndex].position - currentEnemy.transform.position).normalized;

        // Verifica se o inimigo chegou ao ponto de patrulha atual
        if (Vector3.Distance(currentEnemy.transform.position, patrolPoints[currentPatrolIndex].position) < 0.1f)
        {
            // Avance para o pr�ximo ponto de patrulha
            currentPatrolIndex = (currentPatrolIndex + 1) % patrolPoints.Length;

            // Se chegou ao �ltimo ponto de patrulha, desative o inimigo
            if (currentPatrolIndex == 0)
            {
                isPatrolling = false;
                Destroy(currentEnemy);
                return;
            }
        }

        // Mova-se em dire��o ao ponto de patrulha atual
        currentEnemy.transform.position = Vector3.MoveTowards(currentEnemy.transform.position, patrolPoints[currentPatrolIndex].position, movementSpeed * Time.deltaTime);

        // Rotaciona o inimigo para olhar na dire��o do pr�ximo ponto de patrulha
        if (direction != Vector3.zero)
        {
            Quaternion targetRotation = Quaternion.LookRotation(direction);
            currentEnemy.transform.rotation = Quaternion.Slerp(currentEnemy.transform.rotation, targetRotation, 10f * Time.deltaTime);
        }
    }
}
