using UnityEngine;

public class BloodCameraSwitcher : MonoBehaviour
{
    public Camera mainCamera; // C�mera principal do jogo
    public Camera Bcamera; // C�mera do corredor 1
    public AudioClip audioClip; // Clip de �udio a ser reproduzido
    public AudioSource audioSource; // Componente de AudioSource

    private void OnTriggerEnter(Collider other)
    {
        // Verifica se o objeto que entrou no trigger � o jogador
        if (other.gameObject.CompareTag("Player"))
        {
            // Obt�m ou adiciona um AudioSource ao objeto
            //audioSource = GetComponent<AudioSource>();
            if (audioSource == null)
                audioSource = gameObject.AddComponent<AudioSource>();

            // Configura o clip de �udio
            audioSource.clip = audioClip;

            // Ativa a c�mera correspondente ao trigger
            if (gameObject.CompareTag("Trigger3"))
            {
                mainCamera.gameObject.SetActive(false);
                Bcamera.gameObject.SetActive(true);
                // Reproduz o �udio
                audioSource.Play();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        // Verifica se o objeto que saiu do trigger � o jogador
        if (other.gameObject.CompareTag("Player"))
        {
            // Desativa as c�meras do corredor quando o jogador sai do trigger
            Bcamera.gameObject.SetActive(false);
            // Ativa a c�mera principal
            mainCamera.gameObject.SetActive(true);

            // Para a reprodu��o do �udio
            if (audioSource != null)
                audioSource.Stop();
        }
    }
}
