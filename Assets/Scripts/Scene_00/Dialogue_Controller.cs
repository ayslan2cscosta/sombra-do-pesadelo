using UnityEngine;
using System.Collections;

public class Dialogue_Controller : MonoBehaviour
{
    public GameObject objetoADesativar;
    public GameObject objetoAAtivar;
    public Camera novaCamera;
    public Camera faceCamera;

    private void Start()
    {
        StartCoroutine(TrocarObjetosDepoisDeTempo());
    }

    IEnumerator TrocarObjetosDepoisDeTempo()
    {
        yield return new WaitForSeconds(24f); // Espera 30 segundos

        if (objetoADesativar != null)
            objetoADesativar.SetActive(false); // Desativa o objeto

        if (novaCamera != null)
            novaCamera.gameObject.SetActive(true); // Ativa a nova c�mera

        if (faceCamera != null)
            faceCamera.gameObject.SetActive(true); // Ativa a nova c�mera

        if (objetoAAtivar != null)
            objetoAAtivar.SetActive(true); // Ativa o novo objeto
    }
}
