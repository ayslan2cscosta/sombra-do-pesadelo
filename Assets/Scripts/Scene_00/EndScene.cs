using UnityEngine;
using UnityEngine.SceneManagement;

public class EndScene : MonoBehaviour
{
    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    public CanvasGroup caughtBackgroundImageCanvasGroup;
    public AudioSource caughtAudio;
    public AudioSource sadAudio;

    public GameObject objetoADesativar;
    public Camera faceCamera;


    bool m_IsPlayerAtExit;
    bool m_IsPlayerCaught;
    float m_Timer;
    bool m_HasAudioPlayed;
    private bool start = false;

    public void comeco()
    {
        start = true;

    }

    private void Update()
    {
        if (start)
        {
            EndLevel();
        }
    }

    public void EndLevel()
    {
        if (!m_HasAudioPlayed)
        {
            sadAudio.Stop();
            caughtAudio.Play();
            m_HasAudioPlayed = true;
        }

        m_Timer += Time.deltaTime;
        caughtBackgroundImageCanvasGroup.alpha = m_Timer / fadeDuration;

        if (m_Timer > fadeDuration + displayImageDuration)
        {


            // Carrega a cena principal
            SceneManager.LoadScene("MainScene");

        }
    }


    public void Transition()
    {

        if (objetoADesativar != null)
            objetoADesativar.SetActive(false); // Desativa o objeto


        if (faceCamera != null)
            faceCamera.gameObject.SetActive(false); // Ativa a nova c�mera

    }
}
