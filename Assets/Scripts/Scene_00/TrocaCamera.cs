using UnityEngine;

public class TrocaCamera : MonoBehaviour
{
    public Camera cameraAtiva;
    public Camera cameraDesativada;


    public void TrocarCameras()
    {
        // Desativando a c�mera atual
        cameraAtiva.gameObject.SetActive(false);

        // Ativando a nova c�mera
        cameraDesativada.gameObject.SetActive(true);

    }
}
