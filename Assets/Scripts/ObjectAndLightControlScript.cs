using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using System.Collections;

public class ObjectAndLightControlScript : MonoBehaviour
{
    [SerializeField] GameObject ControllerObject;
    public GameObject objectToActivate;
    public GameObject objectToDeactivate1;
    public GameObject objectToDeactivate2;
    public Camera cameraToDeactivate;

    public GameObject lightFlickerGameObject;

    public float delayTime = 7f;

    public PostProcessVolume postProcessVolume; // Refer�ncia ao Post-Process Volume
    private Vignette vignette;
    private AmbientOcclusion ambientOcclusion;

    // Vari�veis para armazenar os valores a serem alterados
    public float vignetteIntensity = 0.5f;
    public float ambientOcclusionIntensity = 0.5f;
    public float lightIntensityMin = 1.5f;
    public float lightIntensityMax = 3.5f;
    public void StartRotina()
    {
        StartCoroutine(ActivationAndLightIntensityRoutine());
    }

    IEnumerator ActivationAndLightIntensityRoutine()
    {
        // Move o objeto para cima para n�o ser vis�vel
        Vector3 newPosition = ControllerObject.transform.position + Vector3.up * 30;
        ControllerObject.transform.position = newPosition;

        // Ativar os objetos
        objectToActivate.SetActive(true);
        objectToDeactivate1.SetActive(false);
        objectToDeactivate2.SetActive(false);
        cameraToDeactivate.enabled = false;
        
        // Alterar a intensidade da luz ap�s 7 segundos
        yield return new WaitForSeconds(delayTime);

        // Inverter a ativa��o
        objectToActivate.SetActive(false);
        objectToDeactivate1.SetActive(true);
        objectToDeactivate2.SetActive(true);
        cameraToDeactivate.enabled = true;

        // Configurar os valores de PostProcessControl
        if (postProcessVolume.profile.TryGetSettings(out vignette))
        {
            Debug.Log("Log "+vignette.intensity.value);
            vignette.intensity.value = vignetteIntensity;
        }

        if (postProcessVolume.profile.TryGetSettings(out ambientOcclusion))
        {
            Debug.Log("Log " + ambientOcclusion.intensity.value);
            ambientOcclusion.intensity.value = ambientOcclusionIntensity;
        }

        // Acessar o script Light Flicker e definir os valores de intensidade m�nima e m�xima
        LightFlicker lightFlickerScript = lightFlickerGameObject.GetComponent<LightFlicker>();
        if (lightFlickerScript != null)
        {
            Debug.Log("Log " + lightFlickerScript.lightIntensityMin);
            lightFlickerScript.lightIntensityMin = lightIntensityMin;
            lightFlickerScript.lightIntensityMax = lightIntensityMax;
        }
        else
        {
            Debug.LogError("Light Flicker script not found on the specified GameObject.");
        }
    }
}
