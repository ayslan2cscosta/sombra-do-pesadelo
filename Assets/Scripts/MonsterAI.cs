using UnityEngine;
using UnityEngine.AI;

public class MonsterAI : MonoBehaviour
{
    public Transform player; // Refer�ncia ao transform do jogador
    public float detectionRange = 8.0f; // Dist�ncia em que o monstro detecta o jogador
    public WaypointPatrol waypointPatrol; // Script de patrulha do monstro
    public Animator animator; // Refer�ncia ao componente Animator
    public NavMeshAgent navMeshAgent; // Refer�ncia ao componente NavMeshAgent

    private void Update()
    {
        // Calcular a dist�ncia entre o monstro e o jogador
        float distanceToPlayer = Vector3.Distance(transform.position, player.position);

        if (distanceToPlayer < detectionRange)
        {
            // Desativar o script de patrulha
            waypointPatrol.enabled = false;

            // Desativar o NavMeshAgent
            navMeshAgent.enabled = false;

            // Definir o estado do Animator para Idle
            animator.SetBool("isIdle", true);

            // Olhar para o jogador
            Vector3 directionToPlayer = (player.position - transform.position).normalized;
            Quaternion lookRotation = Quaternion.LookRotation(new Vector3(directionToPlayer.x, 0, directionToPlayer.z));
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5.0f);
        }
        else
        {
            // Ativar o script de patrulha
            waypointPatrol.enabled = true;

            // Ativar o NavMeshAgent
            navMeshAgent.enabled = true;

            // Definir o estado do Animator para n�o Idle
            animator.SetBool("isIdle", false);
        }
    }
}