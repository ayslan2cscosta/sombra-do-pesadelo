using UnityEngine;

public class SkeletonController : MonoBehaviour
{
    private Animator animator;
    private float idleTime;
    private float nextActionTime;
    private int action;

    void Start()
    {
        animator = GetComponent<Animator>();
        SetRandomIdleTime();
    }

    void Update()
    {
        idleTime += Time.deltaTime;

        if (idleTime >= nextActionTime)
        {
            PerformRandomAction();
            SetRandomIdleTime();
        }
    }

    void SetRandomIdleTime()
    {
        idleTime = 0f;
        nextActionTime = Random.Range(1f, 2f); // Tempo aleatório entre 1 e 2 segundos
    }

    void PerformRandomAction()
    {
        action = Random.Range(0, 3); // 0 = Idle, 1 = LookLeft, 2 = LookRight

        switch (action)
        {
            case 0:
                animator.SetBool("isIdle", true);
                animator.SetBool("lookLeft", false);
                animator.SetBool("lookRight", false);
                break;
            case 1:
                animator.SetBool("isIdle", false);
                animator.SetBool("lookLeft", true);
                animator.SetBool("lookRight", false);
                break;
            case 2:
                animator.SetBool("isIdle", false);
                animator.SetBool("lookLeft", false);
                animator.SetBool("lookRight", true);
                break;
        }
    }
}
