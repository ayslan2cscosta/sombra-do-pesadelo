using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private Image healthBarImage; // Refer�ncia � Image do ProgressBar
    private float healthValue = 0f; // Valor atual da vida (de 0 a 1)
    private float increaseAmount = 0.17f; // Quantidade de aumento da barra ao desativar um objeto
    private bool isInvincible = false; // Flag para verificar se o jogador est� invenc�vel
    private float invincibilityTime = 5f; // Tempo de invencibilidade em segundos

    [SerializeField] Anna_PlayerMovement playerMoviment;
    [SerializeField] private Animator playerAnimator; // Refer�ncia ao Animator do jogador
    [SerializeField] private AudioSource audioSource; // Refer�ncia ao AudioSource do jogador
    [SerializeField] private AudioClip damageAudioClip; // Clip de �udio para o dano

    void Start()
    {
        // Recupera o valor de healthValue da classe HealthManager
        healthValue = HealthManager.healthValue;
        UpdateHealthBar();
    }

    public void IncreaseHealth()
    {
        healthValue += increaseAmount;
        // Limita o valor da vida entre 0 e 1
        healthValue = Mathf.Clamp01(healthValue);
        UpdateHealthBar();
    }

    public void DecreaseHealth(float amount)
    {
        if (!isInvincible)
        {
            healthValue -= amount;
            // Limita o valor da vida entre 0 e 1
            healthValue = Mathf.Clamp01(healthValue);
            UpdateHealthBar();
            StartCoroutine(StartInvincibility());

            // Chama o evento para anima��o e som de dano
            //StartCoroutine(StartOnPlayerDamaged());
            
        }
    }

    public float GetHealth()
    {
        Debug.Log(healthValue);
        return healthValue;
    }

    public bool IsInvincible()
    {
        return isInvincible;
    }

    private void UpdateHealthBar()
    {
        GetHealth();
        healthBarImage.fillAmount = healthValue;

        // Atualiza o valor de healthValue na classe HealthManager
        HealthManager.healthValue = healthValue;
        Debug.Log("Valor de healthValue na primeira cena: " + HealthManager.healthValue);
    }

    private IEnumerator StartInvincibility()
    {
        // Ativa o gatilho "isDamage" no Animator do jogador
        playerAnimator.SetBool("isDamage", true);
        playerMoviment.SetPlayerActive(false);

        // Toca o �udio de dano
        if (audioSource != null && damageAudioClip != null)
        {
            audioSource.PlayOneShot(damageAudioClip);
        }
        isInvincible = true;

        yield return new WaitForSeconds(invincibilityTime);

        playerMoviment.SetPlayerActive(true);
        playerAnimator.SetBool("isDamage", false);

        yield return new WaitForSeconds(5);
        isInvincible = false;

    }


}

public static class HealthManager
{
    public static float healthValue = 0f;
}
