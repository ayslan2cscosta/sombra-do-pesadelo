﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Anna_PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 30f;
    public float moveSpeed = 1.6f;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;

    bool playerActive = true; // Variável para controlar se o jogador está ativo ou não

    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        // Obtém o componente AudioSource do objeto
        m_AudioSource = GetComponent<AudioSource>();
    }

    void FixedUpdate()
    {
        if (!playerActive)
            return; // Se o jogador não estiver ativo, sai da função

        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);

        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);

        // Rotaciona o jogador diretamente com base na direção de movimento
        transform.rotation = Quaternion.Slerp(transform.rotation, m_Rotation, turnSpeed * Time.deltaTime);

        // Multiplica o vetor de movimento pela velocidade de movimento desejada
        m_Movement = m_Movement * moveSpeed;

        // Ajusta a velocidade de reprodução do áudio de acordo com a velocidade de movimento
        m_AudioSource.pitch = Mathf.Clamp(m_Movement.magnitude / moveSpeed, 0.5f, 0.7f); // Ajuste os valores mínimo e máximo conforme necessário
    }

    void Update()
    {
        if (!playerActive)
            return; // Se o jogador não estiver ativo, sai da função

        // Use a entrada do jogador para controlar diretamente o movimento do personagem
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * Time.deltaTime);
    }

    // Método para ativar ou desativar o controle do jogador
    public void SetPlayerActive(bool active)
    {
        playerActive = active;
    }

    public bool GetPlayerActive()
    {
        return playerActive;
    }
}
