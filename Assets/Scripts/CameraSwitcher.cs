using UnityEngine;

public class CameraSwitcher : MonoBehaviour
{
    public Camera mainCamera; // C�mera principal do jogo
    public Camera corridorCamera1; // C�mera do corredor 1
    public Camera corridorCamera2; // C�mera do corredor 2

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Colidiu");
        // Verifica se o objeto que entrou no trigger � o jogador
        if (other.gameObject.tag == ("Player"))
        {
            //Debug.Log("Player");
            // Ativa a c�mera correspondente ao trigger
            if (gameObject.CompareTag("Trigger1"))
            {
                mainCamera.gameObject.SetActive(false);
                corridorCamera1.gameObject.SetActive(true);
                corridorCamera2.gameObject.SetActive(false);
            }
            else if (gameObject.CompareTag("Trigger2"))
            {
                mainCamera.gameObject.SetActive(false);
                corridorCamera1.gameObject.SetActive(false);
                corridorCamera2.gameObject.SetActive(true);
            }

            if (gameObject.CompareTag("Trigger4"))
            {
                mainCamera.gameObject.SetActive(false);
                corridorCamera1.gameObject.SetActive(false);
                corridorCamera2.gameObject.SetActive(true);
                GerenciadorDeConquistas.DesbloquearConquista("Encontrar o coelho amaldi�oado");
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        // Verifica se o objeto que saiu do trigger � o jogador
        if (other.gameObject.tag == ("Player"))
        {
            // Desativa as c�meras do corredor quando o jogador sai do trigger
            corridorCamera1.gameObject.SetActive(false);
            corridorCamera2.gameObject.SetActive(false);
            // Ativa a c�mera principal
            mainCamera.gameObject.SetActive(true);
        }
    }
}
