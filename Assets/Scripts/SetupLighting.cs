using UnityEngine;
using UnityEngine.Rendering;

public class SetupLighting : MonoBehaviour
{
    void Start()
    {
        ApplyLightProbes();
    }

    void ApplyLightProbes()
    {
        // Carrega as sondas de luz geradas automaticamente pelo Unity
        LightProbes lightProbes = LightmapSettings.lightProbes;

        if (lightProbes != null)
        {
            // Aplica as sondas de luz
            LightmapSettings.lightProbes = lightProbes;
        }
        else
        {
            Debug.LogWarning("Nenhuma Light Probe foi gerada automaticamente para esta cena.");
        }
    }
}
