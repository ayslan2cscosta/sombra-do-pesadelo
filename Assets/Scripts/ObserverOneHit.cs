using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObserverOneHit : MonoBehaviour
{
    public Transform player;
    public GameEnding gameEnding;
    public HealthBar healthBar;

    bool m_IsPlayerInRange;
    bool isInvulnerable = false;
    float invulnerabilityTime = 4f; // Tempo de invulnerabilidade em segundos

    void OnTriggerEnter(Collider other)
    {
        if (other.transform == player)
        {
            m_IsPlayerInRange = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.transform == player)
        {
            m_IsPlayerInRange = false;
        }
    }

    void Update()
    {
        if (m_IsPlayerInRange && !healthBar.IsInvincible())
        {
            Vector3 direction = player.position - transform.position + Vector3.up;
            Ray ray = new Ray(transform.position, direction);
            RaycastHit raycastHit;

            if (Physics.Raycast(ray, out raycastHit))
            {
                if (raycastHit.collider.transform == player)
                {

                    gameEnding.CaughtPlayer();
                   
                }
            }
        }
    }

    IEnumerator InvulnerabilityCooldown()
    {
        isInvulnerable = true;
        yield return new WaitForSeconds(invulnerabilityTime);
        isInvulnerable = false;
    }
}
