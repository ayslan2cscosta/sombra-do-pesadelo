using UnityEngine;

public class EncontrarCoelhoAmaldicoado : MonoBehaviour
{

    // M�todo para ser chamado quando o jogador encontrar o coelho amaldi�oado
    public void EncontrarCoelho()
    {
        // Ap�s encontrar o coelho amaldi�oado, desbloqueie a conquista
        GerenciadorDeConquistas.DesbloquearConquista("Encontrar o coelho amaldi�oado");
    }
}
