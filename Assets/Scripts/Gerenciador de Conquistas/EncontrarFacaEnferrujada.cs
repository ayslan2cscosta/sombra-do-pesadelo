using UnityEngine;

public class EncontrarFacaEnferrujada : MonoBehaviour
{
    // M�todo para ser chamado quando o jogador encontrar a faca enferrujada
    public void EncontrarFaca()
    {


        // Ap�s encontrar a faca enferrujada, desbloqueie a conquista
        GerenciadorDeConquistas.DesbloquearConquista("Encontrar a faca enferrujada");
    }
}
