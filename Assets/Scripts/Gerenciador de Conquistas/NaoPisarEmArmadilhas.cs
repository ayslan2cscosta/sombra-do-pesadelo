using UnityEngine;

public class NaoPisarEmArmadilhas : MonoBehaviour
{
    private bool armadilhasAtivadas = false;

    // M�todo para ativar as armadilhas
    public void AtivarArmadilhas()
    {
        armadilhasAtivadas = true;
    }

    // M�todo para verificar se todas as armadilhas foram desativadas
    public void VerificarConquista()
    {
        if (!armadilhasAtivadas)
        {
            // Se nenhuma armadilha foi ativada, desbloqueia a conquista
            GerenciadorDeConquistas.DesbloquearConquista("N�o pisar em nenhuma armadilha");
        }
        else
        {
            // Se alguma armadilha foi ativada, a conquista n�o � desbloqueada
            Debug.Log("O jogador pisou em uma armadilha. Conquista n�o desbloqueada.");
        }
    }
}
