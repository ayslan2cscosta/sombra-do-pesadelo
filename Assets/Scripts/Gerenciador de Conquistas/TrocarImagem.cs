using UnityEngine;
using UnityEngine.UI;

public class TrocarImagem : MonoBehaviour
{
    // Imagem que ser� usada para substituir as imagens dos objetos da UI
    public Sprite novaSprite;

    // Array de imagens da UI que voc� quer trocar
    public Image[] imagensUI;

    // Array de controle para verificar se a troca j� ocorreu
    private bool[] trocaOcorreu;

    private void Start()
    {
        // Inicializa o array de controle com o mesmo tamanho que o array de imagens da UI
        trocaOcorreu = new bool[imagensUI.Length];
    }

    private void Update()
    {
        // Itera sobre todas as imagens da UI
        for (int i = 0; i < imagensUI.Length; i++)
        {
            // Verifica se a vari�vel booleana correspondente � verdadeira e se a troca ainda n�o ocorreu
            if (GetConquista(i) && !trocaOcorreu[i])
            {
                // Atribui o novo sprite apenas � imagem correspondente
                imagensUI[i].sprite = novaSprite;

                // Marca a troca como ocorrida
                trocaOcorreu[i] = true;
            }
        }
    }

    // M�todo auxiliar para verificar se uma conquista est� desbloqueada
    private bool GetConquista(int indice)
    {
        switch (indice)
        {
            case 0: return GerenciadorDeConquistas.encontrouBichinhosDePelucia;
            case 1: return GerenciadorDeConquistas.saiuDoAto1SemPerderSanidade;
            case 2: return GerenciadorDeConquistas.interagiuComAssombracaoDoAto1;
            case 3: return GerenciadorDeConquistas.encontrouFacaEnferrujada; 
            case 4: return GerenciadorDeConquistas.naoPerdeuSanidadeNoAto2; 
            case 5: return GerenciadorDeConquistas.encontrarCoelhoAmaldi�oado;
            case 6: return GerenciadorDeConquistas.naoPisouEmArmadilhas;
            case 7: return GerenciadorDeConquistas.saiuDaMansao;
            default:
                Debug.LogWarning("Conquista n�o encontrada para o �ndice: " + indice);
                return false;
        }
    }
}
