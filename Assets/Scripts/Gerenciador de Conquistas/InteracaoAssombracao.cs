using UnityEngine;

public class InteracaoAssombracao : MonoBehaviour
{
    // M�todo para ser chamado quando o jogador interagir com a assombra��o
    public void InteragirComAssombracao()
    {

        // Ap�s iniciar a conversa com a assombra��o, desbloqueie a conquista
        GerenciadorDeConquistas.DesbloquearConquista("Interagir com a assombra��o do ato 1");
    }
}
