using UnityEngine;

public static class GerenciadorDeSanidade
{
    private static float totalDanoTomado = 0; // Total de dano tomado pelo jogador

    // M�todo para verificar se o jogador tomou dano no ato 2
    public static void VerificarSanidade()
    {
        // Se o jogador estiver no ato 2 e tomou dano, a conquista n�o � desbloqueada
        if (totalDanoTomado > 0)
        {
            Debug.Log("O jogador tomou dano no ato 1. Conquista n�o desbloqueada.");
        }
        // Se o jogador estiver no ato 2 e n�o tomou dano, desbloqueia a conquista
        else if (totalDanoTomado == 0)
        {
            GerenciadorDeConquistas.DesbloquearConquista("Sair do ato 1 sem perder pontos de sanidade");
        }
    }


    // M�todo para adicionar o dano tomado ao total de dano tomado pelo jogador
    public static void AdicionarDanoTomado(float dano)
    {
        totalDanoTomado += dano;
        Debug.Log(totalDanoTomado);
    }
}
