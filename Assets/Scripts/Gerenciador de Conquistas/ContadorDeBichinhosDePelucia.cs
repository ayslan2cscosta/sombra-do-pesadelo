using UnityEngine;

public class ContadorDeBichinhosDePelucia : MonoBehaviour
{
    private int contadorDeBichinhos;

    private void Start()
    {
        contadorDeBichinhos = 0;
    }

    // M�todo para ser chamado quando um bichinho de pel�cia � encontrado
    public void EncontrouBichinhoDePelucia()
    {
        contadorDeBichinhos++;

        // Verifica se todos os 6 bichinhos foram encontrados
        if (contadorDeBichinhos >= 6)
        {
            Debug.Log("Conquista desbloqueada: Abra�o de Pel�cia");
            // Se todos foram encontrados, desbloqueia a conquista
            GerenciadorDeConquistas.DesbloquearConquista("Encontrar todos os bichinhos de pel�cia");
        }
    }
}
