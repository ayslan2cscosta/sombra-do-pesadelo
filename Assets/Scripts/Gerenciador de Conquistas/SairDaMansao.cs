using UnityEngine;

public class SairDaMansao : MonoBehaviour
{

    // M�todo para ser chamado quando o jogador sai da mans�o
    public void SaiuDaMansao()
    {

        GerenciadorDeConquistas.DesbloquearConquista("Sair da mans�o");
    }


}
