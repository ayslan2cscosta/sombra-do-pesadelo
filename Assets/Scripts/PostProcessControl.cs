using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PostProcessControl : MonoBehaviour
{
    public PostProcessVolume postProcessVolume; // Refer�ncia ao Post-Process Volume
    private Vignette vignette;
    private AmbientOcclusion ambientOcclusion;

    // Vari�veis para armazenar os valores a serem alterados
    public float vignetteIntensity = 0.5f;
    public float ambientOcclusionIntensity = 0.5f;

    private void Update()
    {
        // Atualiza os valores do Post-Process Volume
        if (postProcessVolume != null)
        {
            if (postProcessVolume.profile.TryGetSettings(out vignette))
            {
                vignette.intensity.value = vignetteIntensity;
            }

            if (postProcessVolume.profile.TryGetSettings(out ambientOcclusion))
            {
                ambientOcclusion.intensity.value = ambientOcclusionIntensity;
            }
        }
    }

    // Fun��es para definir os valores a serem alterados
    public void SetVignetteIntensity(float intensity)
    {
        vignetteIntensity = intensity;
    }

    public void SetAmbientOcclusionIntensity(float intensity)
    {
        ambientOcclusionIntensity = intensity;
    }
}
