using UnityEngine;
using UnityEngine.AI;

public class MonsterAI1 : MonoBehaviour
{
    public Transform player; // Refer�ncia ao transform do jogador
    public float detectionRange = 6.0f; // Dist�ncia em que o monstro detecta o jogador
    public Animator animator; // Refer�ncia ao componente Animator

    private void Update()
    {
        // Calcular a dist�ncia entre o monstro e o jogador
        float distanceToPlayer = Vector3.Distance(transform.position, player.position);

        if (distanceToPlayer < detectionRange)
        {

            // Definir o estado do Animator para Idle
            animator.SetBool("isIdle", true);

            // Olhar na dire��o oposta ao jogador
            Vector3 directionToPlayer = (transform.position - player.position).normalized;
            Quaternion lookRotation = Quaternion.LookRotation(new Vector3(directionToPlayer.x, 0, directionToPlayer.z));
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5.0f);
        }
        else
        {

            // Definir o estado do Animator para n�o Idle
            animator.SetBool("isIdle", false);
        }
    }
}
