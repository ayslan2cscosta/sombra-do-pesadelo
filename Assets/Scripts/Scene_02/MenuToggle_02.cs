using UnityEngine;

public class MenuToggle_02 : MonoBehaviour
{
    public GameObject objetoDoMenu;
    private bool menuAtivo = false;

    void Update()
    {
        // Verifica se a tecla "Q" foi pressionada
        if (Input.GetKeyDown(KeyCode.Q))
        {
            // Inverte o estado do menu
            menuAtivo = !menuAtivo;

            // Ativa ou desativa o objeto do menu
            if (objetoDoMenu != null)
            {
                objetoDoMenu.SetActive(menuAtivo);
            }

            // Pausa ou continua o jogo conforme necessário
            if (menuAtivo)
            {
                Time.timeScale = 0; // Pausa o jogo
                Cursor.lockState = CursorLockMode.None; // Libera o cursor
                Cursor.visible = true; // Torna o cursor visível
            }
            else
            {
                Time.timeScale = 1; // Continua o jogo
                // Bloquear e esconder o cursor do mouse
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
        }
    }
}
