using System.Collections;
using UnityEngine;

public class AtivarDesativarObjeto : MonoBehaviour
{
    public GameObject objetoParaAtivarDesativar;
    public float tempoParaDesativar = 3f;
    private bool ativado = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !ativado)
        {
            StartCoroutine(AtivarDesativar());
            ativado = true;
        }
    }

    IEnumerator AtivarDesativar()
    {
        objetoParaAtivarDesativar.SetActive(true);
        yield return new WaitForSeconds(tempoParaDesativar);
        objetoParaAtivarDesativar.SetActive(false);
    }
}
