using UnityEngine;

public class ArmadilhaSangue : MonoBehaviour
{
    [SerializeField] NaoPisarEmArmadilhas naoPisar;
    // Refer�ncia ao script IA_Moviment
    public IA_Moviment iaMoviment;
    public AudioSource audioSource;
    public AudioClip somArmadilha;

    void OnTriggerEnter(Collider other)
    {
        // Verifica se o objeto que entrou no trigger � o jogador
        if (other.CompareTag("Player"))
        {
            // Reproduz o �udio da armadilha
            audioSource.PlayOneShot(somArmadilha);

            naoPisar.AtivarArmadilhas();

            // Chama o m�todo em IA_Moviment para definir o novo destino
            iaMoviment.DefinirDestinoArmadilhaSangue(transform.position);
        }
    }
}
