using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar_02 : MonoBehaviour
{
    [SerializeField] private Image healthBarImage; // Refer�ncia � Image do ProgressBar
    private float healthValue;

    // Start is called before the first frame update
    void Start()
    {
        healthBarImage.fillAmount = 0f;
        healthValue = HealthManager.healthValue;
        Debug.Log("Valor de healthValue na segunda cena: " + healthValue);
        healthBarImage.fillAmount = healthValue;
    }
    public void DecreaseHealth(float amount)
    {

            healthValue -= amount;
            // Limita o valor da vida entre 0 e 1
            healthValue = Mathf.Clamp01(healthValue);
            UpdateHealthBar();

    }

    public float GetHealth()
    {
        Debug.Log(healthValue);
        return healthValue;
    }

    private void UpdateHealthBar()
    {
        GetHealth();
        healthBarImage.fillAmount = healthValue;

    }

}
