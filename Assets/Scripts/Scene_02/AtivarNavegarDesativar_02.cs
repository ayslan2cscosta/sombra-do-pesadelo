using UnityEngine;
using UnityEngine.AI;
using System.Collections;

public class AtivarNavegarDesativar_02 : MonoBehaviour
{
    public GameObject objetoParaAtivarDesativar;
    public Transform[] waypoints;
    public float distanciaDesativar = 2f;

    private NavMeshAgent navMeshAgent;
    private int waypointIndex = 0;
    private bool ativado = false;

    private void Start()
    {
        navMeshAgent = objetoParaAtivarDesativar.GetComponent<NavMeshAgent>();
        navMeshAgent.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !ativado)
        {
            AtivarNavegar();
            ativado = true;
        }
    }

    private void AtivarNavegar()
    {
        objetoParaAtivarDesativar.SetActive(true);
        navMeshAgent.enabled = true;
        navMeshAgent.SetDestination(waypoints[waypointIndex].position);
    }

    private void Update()
    {
        if (ativado && navMeshAgent.enabled)
        {
            if (navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance)
            {
                if (waypointIndex == 0) // Se est� no primeiro waypoint
                {
                    objetoParaAtivarDesativar.SetActive(false);
                }
                else
                {
                    waypointIndex = (waypointIndex + 1) % waypoints.Length;
                    navMeshAgent.SetDestination(waypoints[waypointIndex].position);
                }
            }
        }
    }
}
