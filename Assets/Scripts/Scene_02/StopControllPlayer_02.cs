using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StopControllPlayer_02 : MonoBehaviour
{
    [SerializeField] Anna_PlayerMovement_02 playerController; // Refer�ncia ao script de controle do jogador
    [SerializeField] Animator playerAnimator; // Refer�ncia ao componente Animator do jogador

    [SerializeField] Camera AnnaCamera;
    [SerializeField] Camera SkeletonCamera;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void StopController()
    {
        playerController.SetPlayerActive(false);
    }

    public void StartController()
    {
        playerController.SetPlayerActive(true);
    }

    public void CameraAnnaOn()
    {
        AnnaCamera.gameObject.SetActive(true);
    }

    public void CameraAnnaOff()
    {
        AnnaCamera.gameObject.SetActive(false);
    }

    public void CameraSkeletonOn()
    {
        SkeletonCamera.gameObject.SetActive(true);
    }

    public void CameraSkeletonOff()
    {
        SkeletonCamera.gameObject.SetActive(false);
    }

    public void StartInteractingAnimation()
    {
        // Ativa a anima��o "isInteracting" do jogador
        playerAnimator.SetBool("isInteracting", true);
    }

    public void StopInteractingAnimation()
    {
        // Desativa a anima��o "isInteracting" do jogador
        playerAnimator.SetBool("isInteracting", false);
    }

    
}
