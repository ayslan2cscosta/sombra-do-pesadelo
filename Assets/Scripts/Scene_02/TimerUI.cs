using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TimerUI : MonoBehaviour
{
    public float tempoInicial = 180.0f; // 3 minutos em segundos
    private float tempoRestante;
    public TextMeshProUGUI textoTempo;
    public Color corTempoEsgotado = Color.red;

    public IA_Moviment iaMoviment;

    void Start()
    {
        tempoRestante = tempoInicial;
        AtualizarUI();
        InvokeRepeating("AtualizarTempo", 1.0f, 1.0f); // Atualiza o tempo a cada segundo
    }

    void AtualizarTempo()
    {
        tempoRestante -= 1.0f;
        if (tempoRestante <= 0.0f)
        {
            // Tempo esgotado
            tempoRestante = 0.0f;
            AtualizarUI();
            textoTempo.color = corTempoEsgotado;
            iaMoviment.DefinirDestinoJogador();
            CancelInvoke(); // Para de atualizar o tempo
        }
        else
        {
            AtualizarUI();
        }
    }

    void AtualizarUI()
    {
        int minutos = Mathf.FloorToInt(tempoRestante / 60);
        int segundos = Mathf.FloorToInt(tempoRestante % 60);
        textoTempo.text = string.Format("{0:00}:{1:00}", minutos, segundos);
    }
}
