using UnityEngine;
using System.Collections;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class IA_Moviment : MonoBehaviour
{
    public Transform Player;
    [SerializeField] Anna_PlayerMovement_02 playerController; // ReferÍncia ao script de controle do jogador

    private NavMeshAgent naveMesh;
    private float DistanciaDoPlayer, DistanciaDoAIPoint;
    private float DistanciaDoRetornoPoint;
    public float DistanciaDePercepcao = 30, DistanciaDeSeguir = 20, DistanciaDeAtacar = 2, VelocidadeDePasseio = 3, VelocidadeDePerseguicao = 6, TempoPorAtaque = 1.5f, DanoDoInimigo = 40;
    private bool VendoOPlayer, PerseguindoAlgo, AtacandoAlgo;
    private bool Voltando = false;
    public Transform[] DestinosAleatorios;
    public Transform DestinosRetorno;
    private int AIPointAtual;
    private float cronometroDaPerseguicao, cronometroAtaque;

    public float fadeDuration = 2f;
    public float displayImageDuration = 3f;

    public CanvasGroup caughtBackgroundImageCanvasGroup;
    [SerializeField] GameObject FinalObject; // Objeto dentro do jogador
    [SerializeField] Camera AnnaCamera;

    public GameEnding_02 gameEnding;
    public HealthBar_02 healthBar;
    public AudioSource caughtAudio;
    public AudioClip clip;

    public AudioSource AnnaAudio;
    public AudioClip Annaclip;
    public AudioSource PerseguicaoAudio;
    public AudioClip PerseguicaoClip;
    private bool AcionadoArmadilha = false;
    private bool Morte = false;
    private bool m_HasAudioPlayed = true;
    private float initialPerseguicaoVolume;

    void Start()
    {
        AIPointAtual = Random.Range(0, DestinosAleatorios.Length);
        naveMesh = GetComponent<NavMeshAgent>();
        initialPerseguicaoVolume = PerseguicaoAudio.volume;
    }

    void Update()
    {
        DistanciaDoPlayer = Vector3.Distance(Player.position, transform.position);
        DistanciaDoAIPoint = Vector3.Distance(DestinosAleatorios[AIPointAtual].position, transform.position);
        DistanciaDoRetornoPoint = Vector3.Distance(DestinosRetorno.position, transform.position);

        if (Voltando)
            return;

        RaycastHit hit;
        if (Physics.Raycast(transform.position, Player.position - transform.position, out hit, DistanciaDePercepcao))
        {
            if (hit.collider.gameObject.CompareTag("Player"))
            {
                VendoOPlayer = true;
                AcionadoArmadilha = false;

            }
            else
            {
                VendoOPlayer = false;
                m_HasAudioPlayed = true;
            }
        }

        if (!VendoOPlayer)
        {
            if (AcionadoArmadilha)
                return;
        }

        if (Morte)
        {
            Matar();

            if (!VendoOPlayer)
            {
                return;
            }
        }

        if (PerseguindoAlgo)
        {
            cronometroDaPerseguicao += Time.deltaTime;
            if (cronometroDaPerseguicao >= 5 && !VendoOPlayer)
            {
                StartCoroutine(FadeOutPerseguicaoAudio());

                PerseguindoAlgo = false;
                cronometroDaPerseguicao = 0;
            }
        }

        if (DistanciaDoPlayer > DistanciaDePercepcao)
        {
            Passear();
        }
        else if (DistanciaDoPlayer <= DistanciaDePercepcao && DistanciaDoPlayer > DistanciaDeSeguir)
        {
            if (VendoOPlayer)
                Olhar();
            else
                Passear();
        }
        else if (DistanciaDoPlayer <= DistanciaDeSeguir && DistanciaDoPlayer > DistanciaDeAtacar)
        {
            if (VendoOPlayer)
                Perseguir();
            else
                Passear();
        }
        else if (DistanciaDoPlayer <= DistanciaDeAtacar)
        {
            Atacar();
        }

        if (DistanciaDoAIPoint <= 2)
        {
            AIPointAtual = Random.Range(0, DestinosAleatorios.Length);
            Passear();
        }

        // Contador de ataque
        if (AtacandoAlgo)
        {
            cronometroAtaque += Time.deltaTime;
            if (cronometroAtaque >= TempoPorAtaque)
            {
                AtacandoAlgo = false;
                cronometroAtaque = 0;
            }
        }
    }

    void Passear()
    {
        PerseguindoAlgo = false;
        AtacandoAlgo = false;
        naveMesh.acceleration = 5;
        naveMesh.speed = VelocidadeDePasseio;
        naveMesh.destination = DestinosAleatorios[AIPointAtual].position;
    }

    void Olhar()
    {
        PerseguindoAlgo = false;
        AtacandoAlgo = false;
        naveMesh.speed = 0;
        transform.LookAt(Player);
    }

    void Perseguir()
    {
        if (m_HasAudioPlayed)
        {
            AnnaAudio.Play();
            m_HasAudioPlayed = false;
            PerseguicaoAudio.volume = initialPerseguicaoVolume;
            //PerseguicaoAudio.Play();
        }

        PerseguindoAlgo = true;
        AtacandoAlgo = false;
        naveMesh.acceleration = 8;
        naveMesh.speed = VelocidadeDePerseguicao;
        naveMesh.destination = Player.position;
    }

    void Matar()
    {
        naveMesh.acceleration = 10;
        naveMesh.speed = VelocidadeDePasseio;
        naveMesh.destination = Player.position;
    }

    void Atacar()
    {
        AtacandoAlgo = true;

        playerController.SetPlayerActive(false);
        AnnaCamera.gameObject.SetActive(false);
        PerseguicaoAudio.Stop();
        StartCoroutine(SwitchObjectsWithDelay6());
    }

    IEnumerator SwitchObjectsWithDelay6()
    {
        FinalObject.SetActive(true);

        AtacandoAlgo = false;
        Voltando = true;
        naveMesh.speed = 4.2f;
        naveMesh.destination = DestinosRetorno.position;

        yield return new WaitForSeconds(3f);

        if (healthBar.GetHealth() <= 0f)
        {
            gameEnding.CaughtPlayer();
        }
        else
        {
            healthBar.DecreaseHealth(0.17f);
            GerenciadorDeSanidadeAto2.AdicionarDanoTomado(0.17f);
            caughtAudio.PlayOneShot(clip);
        }

        FinalObject.SetActive(false);
        playerController.SetPlayerActive(true);
        AnnaCamera.gameObject.SetActive(true);

        yield return new WaitUntil(() => naveMesh.remainingDistance < 2f);

        Passear();
        Voltando = false;
    }

    IEnumerator FadeOutPerseguicaoAudio()
    {
        float startVolume = PerseguicaoAudio.volume;
        float timer = 0.0f;

        while (timer < fadeDuration)
        {
            timer += Time.deltaTime;
            PerseguicaoAudio.volume = Mathf.Lerp(startVolume, 0.0f, timer / fadeDuration);
            yield return null;
        }

        PerseguicaoAudio.volume = initialPerseguicaoVolume;
        PerseguicaoAudio.Stop();
    }

    public void DefinirDestinoArmadilhaSangue(Vector3 destino)
    {
        AcionadoArmadilha = true;
        naveMesh.destination = destino;
        naveMesh.acceleration = 5;
        naveMesh.speed = VelocidadeDePasseio;
        StartCoroutine(SwitchObjectsWithDelay());
    }

    IEnumerator SwitchObjectsWithDelay()
    {
        yield return new WaitUntil(() => naveMesh.remainingDistance < 1f);

        Passear();

        yield return new WaitForSeconds(1.0f);
        AcionadoArmadilha = false;
    }

    public void DefinirDestinoJogador()
    {
        Morte = true;
    }
}
