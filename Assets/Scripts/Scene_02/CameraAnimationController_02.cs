using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAnimationController_02 : MonoBehaviour
{
    [SerializeField] Camera playerCamera;
    [SerializeField] Anna_PlayerMovement_02 playerController; // Refer�ncia ao script de controle do jogador
    [SerializeField] GameObject colisor;
    [SerializeField] GameObject UISanidade;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void startCamera1Animation()
    {
        Debug.Log("Camera anima��o iniciada");
        // Desativar o controle do jogador no in�cio da cena
        playerController.SetPlayerActive(false);
        UISanidade.SetActive(false);
    }

    public void EndCamera1Animation()
    {
        Debug.Log("Camera anima��o finalizada");
        playerCamera.gameObject.SetActive(true);

        // Ativar o controle do jogador ap�s a anima��o da c�mera
        playerController.SetPlayerActive(true);

        colisor.SetActive(true);
        UISanidade.SetActive(true);

        this.gameObject.SetActive(false);
    }
}
