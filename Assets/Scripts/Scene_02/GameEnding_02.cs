﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEnding_02 : MonoBehaviour
{
    [SerializeField] NaoPisarEmArmadilhas conquista;
    [SerializeField] SairDaMansao conquista_final;

    public GameObject obj1_Desactive;
    public GameObject obj2_Desactive;
    public GameObject Canvas_Desactive;
    public GameObject Menu_Desactive;
    public GameObject obj3_Activate;

    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    public GameObject player;
    public CanvasGroup exitBackgroundImageCanvasGroup;
    public AudioSource exitAudio;
    public CanvasGroup caughtBackgroundImageCanvasGroup;
    public AudioSource caughtAudio;

    bool m_IsPlayerAtExit;
    bool m_IsPlayerCaught;
    float m_Timer;
    bool m_HasAudioPlayed;
    
    void OnTriggerEnter (Collider other)
    {
        if (other.gameObject == player)
        {
            conquista.VerificarConquista();
            conquista_final.SaiuDaMansao();
            GerenciadorDeSanidadeAto2.VerificarSanidadeAto2();
            m_IsPlayerAtExit = true;
        }
    }

    public void CaughtPlayer ()
    {
        m_IsPlayerCaught = true;
    }

    void Update ()
    {
        if (m_IsPlayerAtExit)
        {
            EndLevel();
        }
        else if (m_IsPlayerCaught)
        {
            EndLevel (caughtBackgroundImageCanvasGroup, true, caughtAudio);
        }
    }

    void EndLevel (CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource)
    {
        if (!m_HasAudioPlayed)
        {
            audioSource.Play();
            m_HasAudioPlayed = true;
        }
            
        m_Timer += Time.deltaTime;
        imageCanvasGroup.alpha = m_Timer / fadeDuration;

        if (m_Timer > fadeDuration + displayImageDuration)
        {
            if (doRestart)
            {
                SceneManager.LoadScene (3);
            }
            else
            {
                Application.Quit ();
            }
        }
    }

    void EndLevel()
    {
        obj1_Desactive.SetActive(false);
        obj2_Desactive.SetActive(false);
        obj3_Activate.SetActive(true);

        Cursor.lockState = CursorLockMode.None; // Libera o cursor
        Cursor.visible = true; // Torna o cursor visível
        Canvas_Desactive.SetActive(false);
        Menu_Desactive.SetActive(false);
        StartCoroutine(ChangeSceneAfterDelay(258)); // 210 segundos = 3 minutos e 30 segundos
    }

    // Método para mudar de cena após um atraso
    private IEnumerator ChangeSceneAfterDelay(float delayInSeconds)
    {
        yield return new WaitForSeconds(delayInSeconds);

        // Carrega a cena principal
        SceneManager.LoadScene(0, LoadSceneMode.Single);

    }


}
