﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Anna_PlayerMovement_02 : MonoBehaviour
{
    public float turnSpeed = 30f;
    public float moveSpeed = 1.6f;
    public float mouseSensitivityX = 2.0f; // Sensibilidade do mouse horizontal
    public float mouseSensitivityY = 2.0f; // Sensibilidade do mouse vertical

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    float m_TurnInput;
    float m_LookInputX;
    float m_LookInputY;
    Quaternion m_Rotation = Quaternion.identity;

    bool playerActive = true;

    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();

        // Bloquear e esconder o cursor do mouse
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void FixedUpdate()
    {
        if (!playerActive)
            return;

        // Input do teclado para movimento
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        // Normalizar a entrada de movimento e verificar se está se movendo
        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();
        bool isWalking = m_Movement.magnitude > 0f;
        m_Animator.SetBool("IsWalking", isWalking);

        // Reproduzir ou parar o som conforme necessário
        if (isWalking && !m_AudioSource.isPlaying)
            m_AudioSource.Play();
        else if (!isWalking)
            m_AudioSource.Stop();

        // Input do mouse para rotação
        m_TurnInput = Input.GetAxis("Mouse X") * mouseSensitivityX;
        m_LookInputY = Input.GetAxis("Mouse Y") * mouseSensitivityY;

        // Aplicar rotação horizontal
        transform.Rotate(Vector3.up, m_TurnInput * turnSpeed * Time.deltaTime);

        // Aplicar rotação vertical à câmera
        m_LookInputY = Mathf.Clamp(m_LookInputY, -89f, 89f); // Limitar a rotação vertical
        m_Rotation *= Quaternion.Euler(-m_LookInputY, 0f, 0f);
        Camera.main.transform.localRotation = m_Rotation;

        // Movimento
        m_Movement = transform.forward * vertical + transform.right * horizontal;
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * moveSpeed * Time.deltaTime);

        // Ajuste do pitch do áudio
        m_AudioSource.pitch = Mathf.Clamp(m_Movement.magnitude / moveSpeed, 0.5f, 0.7f);
    }

    // Método para ativar ou desativar o controle do jogador
    public void SetPlayerActive(bool active)
    {
        playerActive = active;
    }

    public bool GetPlayerActive()
    {
        return playerActive;
    }
}
