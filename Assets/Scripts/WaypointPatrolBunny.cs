using UnityEngine;
using UnityEngine.AI;

public class WaypointPatrolBunny : MonoBehaviour
{
    public NavMeshAgent navMeshAgent;
    public Transform[] waypoints;

    int m_CurrentWaypointIndex;
    bool m_IsPatrolling = true;

    void Start()
    {
        if (waypoints.Length > 0)
        {
            navMeshAgent.SetDestination(waypoints[0].position);
            m_CurrentWaypointIndex = 0;
        }
    }

    void Update()
    {
        if (m_IsPatrolling)
        {
            if (navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance)
            {
                m_CurrentWaypointIndex = (m_CurrentWaypointIndex + 1) % waypoints.Length;
                navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
            }
        }
    }

    public void SetTarget(Vector3 targetPosition)
    {
        // Define o alvo como a posi��o especificada
        navMeshAgent.SetDestination(targetPosition);
        m_IsPatrolling = false; // Para a patrulha enquanto se move em dire��o ao alvo
    }

    public void ResumePatrol()
    {
        m_IsPatrolling = true; // Resume a patrulha
    }
}
