using UnityEngine;

public class QuitGame : MonoBehaviour
{
    public void SairDoJogo()
    {
        // Fecha o jogo
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
    }
}
