# Sombra Do Pesadelo

Projeto DM117 - Pós-graduaçãp INATEL


### Sinopse:  
Em "Sombra do Pesadelo" você assume o papel de Anna, uma jovem que acorda em uma mansão sinistra sem memória de como chegou lá. Assombrações, reflexos de suas vítimas, perseguem-na incessantemente, enquanto a escuridão ameaça consumi-la por completo. Para sobreviver, Anna deve confrontar suas memórias mais sombrias e encontrar lembranças escondidas em brinquedos de pelúcia de sua infância, que ajudam a manter sua sanidade. No entanto, cada passo em direção à verdade a aproxima de um destino incerto. Conseguirá Anna escapar da mansão e de sua própria mente atormentada? Ou será que ela está condenada a vagar eternamente pelos corredores sombrios de sua própria culpa e arrependimento?  

### Destaques:  

- Enredo psicológico.  

- Interação com objetos  de infância.  

- Ambientes  e atmosfera opressiva.  

- Confrontos com assombrações que refletem os traumas de Anna. 
